package OpenCVSample;

import OpenCVSample.businessLogic.User;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ListView;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.scene.paint.Color;


import java.util.HashMap;

public class MainController {

    @FXML
    private ImageView cameraView;

    @FXML
    private Parent mainView;

    @FXML
    private Button logout;

    //bind checkboxes with their firebase field;

    @FXML
    private ListView checkboxList;

    private User currentUser;

    private HashMap<String, Integer> stock;

    private Firebase fb = new Firebase();

    protected void initialize(){
        logout.setStyle("-fx-background-color: #379EC1");
        logout.setFont(Font.loadFont("file:font/roboto/Roboto-Medium.ttf", 12));
        //fill fields objects
        stock = fb.getStock();

        //for each checkbox, check if available and if borrowable

        bindCheckBoxes();
    }


    private void bindCheckBoxes(){

        currentUser.getEquipmentList().forEach((String key, Boolean value)-> {
            Integer tmpQuantity = stock.get(key);
            if(isBorrowable(value, tmpQuantity)){
                CheckBox tmpCheckbox = new CheckBox();
                tmpCheckbox.setText(key);
                tmpCheckbox.setSelected(value);
                tmpCheckbox.setFont(Font.loadFont("file:font/roboto/Roboto-Medium.ttf", 12));
                tmpCheckbox.setStyle("-fx-background-color: #379EC1");

                System.out.print(tmpQuantity);

                checkboxList.getItems().add(tmpCheckbox);
            }
        });
    }

    private Boolean isBorrowable(Boolean wasBorrowed, Integer quantity){

        return (wasBorrowed || quantity > 0);

    }

    public void logout() {
        //destruct current user, save changes

        //update User data and stock data
        updateData();
        fb.sendUserUpdate(currentUser, currentUser.getID());
        fb.updateStock(stock);

        // switch view
        loadView();
    }

    private void updateData() {
        checkboxList.getItems().forEach(( item)->{
            if(item instanceof CheckBox) {

                HashMap<String, Boolean> tmpEquipmentList = currentUser.getEquipmentList();
                CheckBox currentItem = (CheckBox) item;

                if(currentItem.isSelected() != tmpEquipmentList.get(currentItem.getText())){

                    Integer newStockValue = stock.get(currentItem.getText());
                    if(currentItem.isSelected()){
                        newStockValue--;
                    }
                    else{
                        newStockValue ++;
                    }
                    stock.put(currentItem.getText(), newStockValue);
                }

                tmpEquipmentList.put(currentItem.getText(), currentItem.isSelected());

            }
        });
    }

    public void show(Parent main, User user) {

        currentUser = user;

        Scene scene = new Scene(this.mainView);

        Stage stage = new Stage();
        stage.setScene(scene);
        stage.setTitle(currentUser.getUsername());

        stage.setResizable(false);
        stage.show();

        Stage mainStage = (Stage) main.getScene().getWindow();
        mainStage.close();

        initialize();
    }

    private void loadView(){
        try {

            FXMLLoader tmpLoader = new FXMLLoader(getClass().getResource("login.fxml"));
            Parent newView = tmpLoader.load();
            LoginController tmpController = tmpLoader.getController();
            tmpController.show(this.mainView);
        }

        catch(Exception e){
            e.printStackTrace();
        }
    }
}