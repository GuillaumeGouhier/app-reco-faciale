package OpenCVSample.businessLogic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class User {

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    private String username;

    public HashMap<String, Boolean> getEquipmentList() {
        return equipmentList;
    }

    public void setEquipmentList(HashMap<String, Boolean> equipmentList) {
        this.equipmentList = equipmentList;
    }

    private HashMap<String, Boolean> equipmentList;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    private String ID;

    public User(String username){

        this.username = username;
        equipmentList = new HashMap<String, Boolean>();

        equipmentList.put("Mousqueton", false);
        equipmentList.put("Gant", false);
        equipmentList.put("Ceinture", false);
        equipmentList.put("Detecteur", false);
        equipmentList.put("Brassard", false);
        equipmentList.put("Lampe", false);
        equipmentList.put("Gilet", false);

    }
}
