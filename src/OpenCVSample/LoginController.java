package OpenCVSample;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import OpenCVSample.businessLogic.User;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoCapture;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;


public class LoginController
{
    // the FXML button
    @FXML
    private Button login;
    // the FXML image view
    @FXML
    private ImageView cameraView;

    @FXML
    private Parent mainView;

    @FXML
    protected void initialize(){

        Firebase fb = new Firebase();

        fb.getImages();

        recognizer = new FaceRecognition();
        startCamera();

        login.setStyle("-fx-background-color: #379EC1");
        login.setFont(Font.loadFont("file:font/roboto/Roboto-Medium.ttf", 12));
    }

    // a timer for acquiring the video stream
    private ScheduledExecutorService timer;
    // the OpenCV object that realizes the video capture
    private VideoCapture capture = new VideoCapture();
    // a flag to change the button behavior
    private boolean cameraActive = false;
    // the id of the camera to be used
    private static int cameraId = 0;

    private User currentUser;

    FaceRecognition recognizer;

 //   private FaceRecognition recognizer = new FaceRecognition();

    @FXML
    protected void startCamera()
    {
        if (!this.cameraActive)
        {
            // start the video capture
            this.capture.open(cameraId);

            // is the video stream available?
            if (this.capture.isOpened())
            {
                this.cameraActive = true;

                // grab a frame every 33 ms (30 frames/sec)
                Runnable frameGrabber = new Runnable() {

                    @Override
                    public void run()
                    {
                        // effectively grab and process a single frame
                        Mat frame = grabFrame();
                        // convert and show the frame
                        Image imageToShow = Utils.mat2Image(frame);
                        updateImageView(cameraView, imageToShow);
                    }
                };

                this.timer = Executors.newSingleThreadScheduledExecutor();
                this.timer.scheduleAtFixedRate(frameGrabber, 0, 33, TimeUnit.MILLISECONDS);

                // update the button content
            }
            else
            {
                // log the error
                System.err.println("Impossible to open the camera connection...");
            }
        }

    }

    /**
     * Get a frame from the opened video stream (if any)
     *
     * @return the {@link Mat} to show
     */
    private Mat grabFrame()
    {
        // init everything
        Mat frame = new Mat();

        // check if the capture is open
        if (this.capture.isOpened())
        {
            try
            {
                // read the current frame
                this.capture.read(frame);

                // if the frame is not empty, process it
                if (!frame.empty())
                {
                    Imgproc.cvtColor(frame, frame, Imgproc.COLOR_BGR2GRAY);
                }

            }
            catch (Exception e)
            {
                // log the error
                System.err.println("Exception during the image elaboration: " + e);
            }
        }

        return frame;
    }

    /**
     * Stop the acquisition from the camera and release all the resources
     */
    private void stopAcquisition()
    {
        if (this.timer!=null && !this.timer.isShutdown())
        {
            try
            {
                // stop the timer
                this.timer.shutdown();
                this.timer.awaitTermination(33, TimeUnit.MILLISECONDS);
            }
            catch (InterruptedException e)
            {
                // log any exception
                System.err.println("Exception in stopping the frame capture, trying to release the camera now... " + e);
            }
        }

        if (this.capture.isOpened())
        {
            // release the camera
            this.capture.release();
        }
    }

    /**
     * Update the {@link ImageView} in the JavaFX main thread
     *
     * @param view
     *            the {@link ImageView} to update
     * @param image
     *            the {@link Image} to show
     */
    private void updateImageView(ImageView view, Image image)
    {
        Utils.onFXThread(view.imageProperty(), image);
    }

    /**
     * On application close, stop the acquisition from the camera
     */
    protected void setClosed()
    {
        this.stopAcquisition();
    }

    @FXML
    private void login(ActionEvent event){



        Firebase fb = new Firebase();
        //call facerecognition
        Mat frame = new Mat();
        capture.read(frame);

        String username = recognizer.detectAndDisplay(frame);;

        //load next scene or send popup

        if(username == null || username == "Unknown"){
            displayPopup();
        }

        else{
            currentUser = fb.getUser(username);
            currentUser.setID(username);
            loadView();
        }

    }

    private void displayPopup(){
        Stage popupwindow=new Stage();

        popupwindow.initModality(Modality.APPLICATION_MODAL);
        popupwindow.setTitle("Erreur");


        Label label1= new Label("User not known");


        Button button1= new Button("Fermer");


        button1.setOnAction(e -> popupwindow.close());

        VBox layout= new VBox(10);


        layout.getChildren().addAll(label1, button1);

        layout.setAlignment(Pos.CENTER);

        Scene scene1= new Scene(layout, 300, 250);

        popupwindow.setScene(scene1);

        popupwindow.showAndWait();
    }

    private void loadView(){
        try {

            stopAcquisition();

            FXMLLoader tmpLoader = new FXMLLoader(getClass().getResource("main.fxml"));
            Parent newView = tmpLoader.load();
            MainController tmpController = tmpLoader.getController();
            tmpController.show(this.mainView, currentUser);
        }

        catch(Exception e){
            e.printStackTrace();
        }
    }

    public void show(Parent main) {

        Scene scene = new Scene(this.mainView);

        Stage stage = new Stage();
        stage.setScene(scene);
        stage.setTitle("MSPR");
        stage.setResizable(false);
        stage.show();

        Stage mainStage = (Stage)main.getScene().getWindow();
        mainStage.close();
    }

}

