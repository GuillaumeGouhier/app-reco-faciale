package OpenCVSample;

import OpenCVSample.businessLogic.User;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONObject;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


public class Firebase
{
    String httpsURL = "https://firestore.googleapis.com/v1/projects/gosecuri-64593/databases/(default)/documents/user/";

    public void sendUserUpdate (User user, String userID) {
        try {

            HttpClient client = HttpClients.createDefault();

            JSONObject fb_document = new JSONObject();

            //HashMap to Json (firebase format)
            JSONObject tmpfields = new JSONObject();
            JSONObject equipmentList = new JSONObject();
            JSONObject mapValue = new JSONObject();
            JSONObject fields = new JSONObject();
            user.getEquipmentList().forEach((String key, Boolean value)-> {

                JSONObject tmp = new JSONObject();
                tmp.put("booleanValue", value);
                fields.put(key, tmp);

            });

            mapValue.put("fields", fields);
            equipmentList.put("mapValue", mapValue);

            tmpfields.put("equipmentList", equipmentList);

            //Username to firebase json

            JSONObject username = new JSONObject();
            username.put("stringValue", user.getUsername());

            tmpfields.put("username", username);

            fb_document.put("name", "projects/gosecuri-64593/databases/(default)/documents/user/".concat(userID));
            fb_document.put("fields",tmpfields);

            String json = fb_document.toString();
            System.out.print(json);
            HttpPatch request = new HttpPatch(httpsURL.concat(userID));
            StringEntity params = new StringEntity(json);
            params.setContentType("application/json");

            request.setEntity(params);
            HttpResponse response = client.execute(request);
            System.out.print(response.getStatusLine().getStatusCode());String line;
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            while ((line = rd.readLine()) != null) {
                System.out.println(line);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public User getUser(String userID) {

        User result;

        try {


            URL firebase_database = new URL("https://firestore.googleapis.com/v1/projects/gosecuri-64593/databases/(default)/documents/user/".concat(userID));
            HttpsURLConnection connection = (HttpsURLConnection) firebase_database.openConnection();

            //request response to string

            StringBuilder acc = new StringBuilder();
            InputStream is = connection.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);

            String inputline;
            while ((inputline = br.readLine()) != null) {
                acc.append(inputline);
            }

            //String to json

            JSONObject response = new JSONObject(acc.toString());
            System.out.print("\n\n");
            JSONObject userJSON = response.getJSONObject("fields");
            System.out.print(userJSON);

//Response to User

            result = new User(userJSON.getJSONObject("username").get("stringValue").toString());
            JSONObject equipmentList = (JSONObject)userJSON.getJSONObject("equipmentList").getJSONObject("mapValue").getJSONObject("fields");

            HashMap<String, Boolean> tmpEquipmentList = new HashMap<String, Boolean>();
            result.getEquipmentList().forEach((String key, Boolean value)->{
                tmpEquipmentList.put(key, (Boolean)equipmentList.getJSONObject(key).get("booleanValue"));
                System.out.print(tmpEquipmentList.get(key));
            });

            result.setEquipmentList(tmpEquipmentList);

            System.out.print("\n\n");
            System.out.print(result.getEquipmentList().get("Gant"));

            return result;
        }
        catch (Exception e){
            System.out.print(e.toString());
        }

        return null;
    }

   public HashMap<String, Integer> getStock(){
       URL firebase_database = null;
       try {
           firebase_database = new URL("https://firestore.googleapis.com/v1/projects/gosecuri-64593/databases/(default)/documents/user/stock");
       HttpsURLConnection connection = (HttpsURLConnection) firebase_database.openConnection();

       //request response to string

       StringBuilder acc = new StringBuilder();
       InputStream is = connection.getInputStream();
       InputStreamReader isr = new InputStreamReader(is);
       BufferedReader br = new BufferedReader(isr);

       String inputline;
       while ((inputline = br.readLine()) != null) {
           acc.append(inputline);
       }

       //String to json

       JSONObject response = new JSONObject(acc.toString());

       JSONObject data = response.getJSONObject("fields");
       System.out.print(data.toString());
       HashMap<String, Integer> result = new HashMap<String, Integer>();
       Iterator<String> keys = data.keys();

       while(keys.hasNext()){
           String key = keys.next();
           Integer value = Integer.parseInt((String) data.getJSONObject(key).get("integerValue"));
           result.put(key, value);
       }
       return result;

       } catch (Exception e) {
           e.printStackTrace();
       }
       return null;
    }

   public void updateStock(HashMap<String, Integer> stock){
        try {

            HttpClient client = HttpClients.createDefault();

            JSONObject fb_document = new JSONObject();

            //HashMap to Json (firebase format)
            JSONObject fields = new JSONObject();
            stock.forEach((String key, Integer value)-> {

                JSONObject tmp = new JSONObject();
                tmp.put("integerValue", value);
                fields.put(key, tmp);

            });

            fb_document.put("name", "projects/gosecuri-64593/databases/(default)/documents/user/stock");
            fb_document.put("fields",fields);

            String json = fb_document.toString();
            System.out.print(json);
            HttpPatch request = new HttpPatch(httpsURL.concat("stock"));
            StringEntity params = new StringEntity(json);
            params.setContentType("application/json");

            request.setEntity(params);
            HttpResponse response = client.execute(request);
            System.out.print(response.getStatusLine().getStatusCode());String line;
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            while ((line = rd.readLine()) != null) {
                System.out.println(line);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void saveImage(String imageUrl, String destinationFile) throws IOException {
        URL url = new URL(imageUrl);
        InputStream is = url.openStream();
        OutputStream os = new FileOutputStream(destinationFile);

        byte[] b = new byte[2048];
        int length;

        while ((length = is.read(b)) != -1) {
            os.write(b, 0, length);
        }

        is.close();
        os.close();
    }

    public void getImages(){

        try {
            URL firebase_database = new URL("https://firestore.googleapis.com/v1/projects/gosecuri-64593/databases/(default)/documents/user/reco_faciale");
            HttpsURLConnection connection = (HttpsURLConnection) firebase_database.openConnection();

            //request response to string

            StringBuilder acc = new StringBuilder();
            InputStream is = connection.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);


            String inputline;
            while ((inputline = br.readLine()) != null) {
                acc.append(inputline);
            }

            //String to json

            JSONObject response = new JSONObject(acc.toString());

            List<Object> data = response.getJSONObject("fields").getJSONObject("images").getJSONObject("arrayValue").getJSONArray("values").toList();

            List<String> urls = new ArrayList<String>();

            data.forEach((item)->{

                HashMap tmp = (HashMap) item;
                urls.add((String) tmp.get("stringValue"));

            });

            urls.forEach((item)->{
                System.out.print("\n");

                Integer begin = item.lastIndexOf("%")+3;

                Integer end = item.lastIndexOf("?");

                String path = "data/train_data/".concat(item.substring(begin, end));

                try {
                    saveImage(item, path);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
